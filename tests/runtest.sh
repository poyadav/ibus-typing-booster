#!/bin/bash
. /usr/share/beakerlib/beakerlib.sh || exit 1

NAME=ibus-typing-booster

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm ${NAME}
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "pushd $tmp"
        if ! rlCheckRpm gnome-desktop-testing; then
            echo "gnome-desktop-testing is not available."
            echo "Trying to download and compile it:"
            git clone https://gitlab.gnome.org/GNOME/gnome-desktop-testing.git
            pushd gnome-desktop-testing
            ./autogen.sh --prefix=/usr --sysconfdir=/etc --localstatedir=/var
            make
            make install
            popd
        fi
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "ibus-desktop-testing-runner \
          --no-graphics \
          --runner=gnome \
          --timeout=1500 \
          --tests=${NAME} \
          --output=${NAME}.log \
          --result=test.log \
          " 0 "Running ${NAME} tests"
        echo "==== ${NAME}.log: ===="
        cat ${NAME}.log
        echo "==== EOF ${NAME}.log: ===="
        echo "==== test.log ===="
        cat test.log
        echo "==== EOF test.log: ===="
        rlAssertNotGrep FAIL test.log
        rlFileSubmit ${NAME}.log
        rlFileSubmit test.log
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
